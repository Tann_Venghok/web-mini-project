import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";

export default class Card1 extends Component {
  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.close}>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.people.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Gender: {this.props.people.gender}</p>
          <div>
            <ul>
              <li className={this.props.people.student ? "" : "d-none"}>
                Student
              </li>
              <li className={this.props.people.teacher ? "" : "d-none"}>
                Teacher
              </li>
              <li
                className={this.props.people.developer ? "" : "d-none"}
              >
                Developer
              </li>
            </ul>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.close}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
