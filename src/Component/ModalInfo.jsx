import React from "react";
import { Button, Modal } from "react-bootstrap";
export default function ModalInfo(props) {
  const [show, setShow] = React.useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <span>
      <Button variant="primary" onClick={handleShow}>
        View
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{props.people.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Gender: {props.people.gender}</p>{" "}
          <div>
            <p>Jobs:</p>
            <ul>
              <li className={props.people.student ? "" : "d-none"}>Student</li>
              <li className={props.people.teacher ? "" : "d-none"}>Teacher</li>
              <li className={props.people.developer ? "" : "d-none"}>
                Developer
              </li>
            </ul>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </span>
  );
}
