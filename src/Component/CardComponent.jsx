import React, { Component } from "react";
import { Card, DropdownButton, Dropdown } from "react-bootstrap";
import Card1 from "./Card1";
import Moment from "react-moment";
import "moment/locale/km";

export default class CardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
    };
  }
  show() {
    this.setState({ show: true });
  }
  close() {
    this.setState({ show: false });
  }
  render() {
    var time = new Date(this.props.people.createTime);
    return (
      <Card style={{ height: "100%" }}>
        <Card.Header className="text-center">
          <DropdownButton id="dropdown-basic-button" title="Action">
            <Dropdown.Item href="#/action-1" onClick={this.show.bind(this)}>
              View
            </Dropdown.Item>
            <Card1
              show={this.state.show}
              close={this.close.bind(this)}
              people={this.props.people}
            />
            <Dropdown.Item
              href="#/action-3"
              onClick={() => this.props.onUpdate(this.props.people, this.props.index)}
            >
              Update
            </Dropdown.Item>
            <Dropdown.Item
              href="#/action-2"
              onClick={() => this.props.onDelete(this.props.id)}
            >
              Delete
            </Dropdown.Item>
          </DropdownButton>
        </Card.Header>
        <Card.Body>
          <h3>{this.props.people.name}</h3>
          <h5>Jobs:</h5>
          <ul>
            <li className={this.props.people.student ? "" : "d-none"}>
              Student
            </li>
            <li className={this.props.people.teacher ? "" : "d-none"}>
              Teacher
            </li>
            <li className={this.props.people.developer ? "" : "d-none"}>
              Developer
            </li>
          </ul>
        </Card.Body>
        <Card.Footer className="text-muted text-center">
          <Moment fromNow locale="km">{time}</Moment>
        </Card.Footer>
      </Card>
    );
  }
}
