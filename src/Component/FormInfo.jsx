import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import List from "./List";
import CardComponent from "./CardComponent";
import "react-moment";

let randomString = require("randomstring");
let i = 1;
const nameRegex = RegExp(/^[a-zA-Z\s]*$/);
export default class FormInfo extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      age: "",
      gender: "male",
      student: false,
      teacher: false,
      developer: false,
      createTime: "",
      updateTime: "",
      people: [],
      isUpdate: false,
      indexUpdate: null,
      cardList: "list",
    };
    // this.submit = this.submit.bind(this);
  }
  componentDidMount() {
    this.nameInput.focus();
  }
  //name
  handleNameChange = (event) => {
    this.setState({
      name: event.target.value,
    });
  };
  //age
  handleAgeChange = (event) => {
    this.setState({
      age: event.target.value,
    });
    console.log(this.state.age);
  };
  handleRadioButton = (event) => {
    if (event.target.checked) {
      this.setState({
        gender: event.target.value,
      });
    }
  };
  //start cheackbox
  handleCheck = (event) => {
    this.setState({
      student: event.target.checked ? "Student" : "",
    });
  };
  handleCheck1 = (event) => {
    this.setState({
      teacher: event.target.checked ? "Teacher" : "",
    });
  };
  handleCheck2 = (event) => {
    this.setState({
      developer: event.target.checked ? "Developer" : "",
    });
  };
  //end checkbox

  validateForm() {
    var name = this.state.name;
    var age = this.state.age;
    if (name !== "" && age !== "") {
      if (nameRegex.test(name) && age.length < 3 && age>0) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  submit() {
    let date = new Date();
    let myTime = date.toISOString();
    // if (!this.formValid(this.state.formError)) {
    if (this.validateForm()) {
      if (this.state.isUpdate === false) {
        var Object = {
          No: i++,
          id: randomString.generate({
            length: 4,
            charset: "alphanumeric",
          }),
          name: this.state.name,
          age: this.state.age,
          gender: this.state.gender,
          student: this.state.student,
          teacher: this.state.teacher,
          developer: this.state.developer,
          createTime: myTime,
          updateTime: myTime,
          isUpdate: this.state.isUpdate,
        };
        this.setState({
          people: [...this.state.people, Object],
        });
        this.nameInput.focus();
      } else if (this.state.isUpdate === true) {
        let people = [...this.state.people];
        people[this.state.indexUpdate].name = this.state.name;
        people[this.state.indexUpdate].age = this.state.age;
        people[this.state.indexUpdate].gender = this.state.gender;
        people[this.state.indexUpdate].student = this.state.student;
        people[this.state.indexUpdate].teacher = this.state.teacher;
        people[this.state.indexUpdate].developer = this.state.developer;
        people[this.state.indexUpdate].updateTime = myTime;
        this.setState({
          people,
          isUpdate: false,
        });
        this.nameInput.focus();
      }
      this.setState({
        name: "",
        age: "",
      });
    } else {
      alert("can't submit");
    }
  }
  handleDelete = (No) => {
    const person = this.state.people.filter((person) => person.No !== No);
    this.setState({
      people: person,
    });
  };
  handleUpdate = (person, index) => {
    let newIndex = this.state.people.findIndex((per) => per.No === person.No);
    this.setState({
      name: person.name,
      age: person.age,
      gender: person.gender,
      student: person.student,
      teacher: person.teacher,
      developer: person.developer,
      isUpdate: !person.isUpdate,
      indexUpdate: newIndex,
    });
    this.nameInput.focus();
    // index=person.No-1;

    console.log(this.state.isUpdate);
  };

  ListChange = () => {
    this.setState({
      cardList: "list",
    });
  };

  CardChange = () => {
    this.setState({
      cardList: "card",
    });
  };
  render() {
    return (
      <div className="container">
        <div style={{ backgroundColor: "black" }}>
          <h1 style={{ textAlign: "center", padding: "12px", color: "white" }}>
            Personal Information
          </h1>
        </div>
        <div className="row">
          <div className="col-lg-8">
            <Form>
              <Form.Group>
                <Form.Label>Name:</Form.Label>
                <Form.Control
                  type="text"
                  name="Name"
                  value={this.state.name}
                  placeholder="Input Name"
                  onChange={this.handleNameChange}
                  ref={(input) => {
                    this.nameInput = input;
                  }}
                />
                <small style={{ color: "red" }}>
                  {nameRegex.test(this.state.name) ? "" : "Invalid name"}
                </small>
              </Form.Group>
              <Form.Group>
                <Form.Label>Age:</Form.Label>
                <Form.Control
                  type="number"
                  name="Age"
                  value={this.state.age}
                  placeholder="Input Age"
                  onChange={this.handleAgeChange}
                />
                <small style={{ color: "red" }}>
                  {this.state.age.length >= 3 ? "Invalid number" : ""}
                </small>
              </Form.Group>
            </Form>
          </div>
          <div className="col-lg-4">
            <h4>Gender:</h4>
            <input
              id="id"
              onChange={this.handleRadioButton}
              checked={this.state.gender === "male"}
              type="radio"
              name="gender"
              value="male"
            />
            <Form.Label>Male</Form.Label>
            <input
              style={{ marginLeft: "15px" }}
              onChange={this.handleRadioButton}
              checked={this.state.gender === "female"}
              type="radio"
              name="gender"
              value="female"
            />
            <Form.Label>Female</Form.Label>
            <h4>Job:</h4>
            <input
              onChange={this.handleCheck}
              checked={this.state.student}
              type="checkbox"
              name="job1"
              value="Student"
            />
            <Form.Label>Student</Form.Label>
            <input
              style={{ marginLeft: "15px" }}
              onChange={this.handleCheck1}
              checked={this.state.teacher}
              type="checkbox"
              name="job2"
              value="Teacher"
            />
            <Form.Label>Teacher</Form.Label>
            <input
              style={{ marginLeft: "15px" }}
              onChange={this.handleCheck2}
              checked={this.state.developer}
              type="checkbox"
              name="job3"
              value="Developer"
            />
            <Form.Label>Developer</Form.Label>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <Button
              variant="dark"
              onClick={() => this.submit(this.state.people.No)}
            >
              {this.state.isUpdate ? "Update" : "Submit"}
            </Button>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <center>
              <label>Display data:</label>
              {"  "}
              <Button variant="secondary" onClick={() => this.ListChange()}>
                List
              </Button>
              {"  "}
              <Button variant="secondary" onClick={() => this.CardChange()}>
                Card
              </Button>
            </center>
          </div>
        </div>
        <div className="row" style={{ marginTop: "10px" }}>
          <div className="col-lg-12">
            {this.state.cardList === "list" ? (
              <List
                className="col-lg-12"
                people={this.state.people}
                onDelete={this.handleDelete}
                onUpdate={this.handleUpdate}
              />
            ) : (
              this.state.people.map((person, index) => (
                <div className="col-lg-3 d-inline-block" key={index}>
                  <CardComponent
                    id={person.No}
                    people={person}
                    index={index}
                    onDelete={this.handleDelete}
                    onUpdate={this.handleUpdate}
                  />
                </div>
              ))
            )}
          </div>
        </div>
      </div>
    );
  }
}
