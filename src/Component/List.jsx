import React, { Component } from "react";
import { Button, Table } from "react-bootstrap";
import PaginationInfo from "./PaginationInfo";
import ModalInfo from "./ModalInfo";
import "moment/locale/km";
import Moment from "react-moment";

export default class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      postsPerPage: 3,
    };
    console.log(this.props.people);
  }
  viewItem() {
    this.setState({
      show: true,
    });
  }
  closeItem() {
    this.setState({
      show: false,
    });
  }
  render() {
    const { currentPage, postsPerPage } = this.state;
    const indexOfLastPage = currentPage * postsPerPage;
    const indexOfFirstPage = indexOfLastPage - postsPerPage;
    const currentPost = this.props.people.slice(
      indexOfFirstPage,
      indexOfLastPage
    );
    // console.log(currentPost)
    let people1 = currentPost.map((person, index) => {
      return (
        <tr key={person.No}>
          <td>{person.id}</td>
          <td>{person.name}</td>
          <td>{person.age}</td>
          <td>{person.gender}</td>
          <td>
            <ul>
              <li className={person.student ? "" : "d-none"}>Student</li>
              <li className={person.teacher ? "" : "d-none"}>Teacher</li>
              <li className={person.developer ? "" : "d-none"}>
                Developer
              </li>
            </ul>
          </td>
          <td>
            <Moment fromNow locale="km">
              {person.createTime}
            </Moment>
          </td>
          <td>
            <Moment fromNow locale="km">
              {person.updateTime}
            </Moment>
          </td>
          <td>
            <ModalInfo people={person} />
            {"  "}
            <Button
              variant="warning"
              onClick={() => this.props.onUpdate(person, index)}
            >
              Update
            </Button>{" "}
            <Button
              variant="danger"
              onClick={() => this.props.onDelete(person.No)}
            >
              Delete
            </Button>
          </td>
        </tr>
      );
    });
    const paginate = (pageNumber) => {
      this.setState({
        currentPage: pageNumber,
      });
    };
    const nextPage = () => {
      if (currentPage >=
         this.props.people.length / postsPerPage) {
        this.setState({
          currentPage: currentPage,
        });
      } else {
        this.setState({
          currentPage: currentPage + 1,
        });
      }
    };
    const PrevPage = () => {
      if (currentPage <= 1) {
        this.setState({
          currentPage: 1,
        });
      } else {
        this.setState({
          currentPage: currentPage - 1,
        });
      }
    };
    return (
      <div>
        <Table hover size="sm">
          <thead>
            <tr className="bg-secondary text-white text-center">
              <th>ID</th>
              <th>Name</th>
              <th>Age</th>
              <th>Gender</th>
              <th>Job</th>
              <th>CreateAt</th>
              <th>UpdateAt</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{people1}</tbody>
        </Table>
        <PaginationInfo
          postsPerPage={postsPerPage}
          totalPosts={this.props.people.length}
          paginate={paginate}
          nextPage={nextPage}
          prevPage={PrevPage}
        />
      </div>
    );
  }
}
