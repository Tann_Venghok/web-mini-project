import React, { Component } from "react";

export default class PaginationInfo extends Component {
  render() {
    const {
      postsPerPage,
      totalPosts,
      paginate,
      nextPage,
      prevPage,
    } = this.props;
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
      pageNumbers.push(i);
    }

    return (
      <nav>
        <ul className="pagination justify-content-center">
          <li className="page-item">
            <button
              className="page-link"
              // disabled={false}
              href="!#"
              onClick={() => prevPage()}
            >
              Previous
            </button>
          </li>
          {pageNumbers.map((num) => (
            <li className="page-item" key={num}>
              <button
                onClick={() => paginate(num)}
                href="!#"
                className="page-link"
              >
                {num}
              </button>
            </li>
          ))}
          <li className="page-item">
            <button className="page-link" href="!#" onClick={() => nextPage()}>
              Next
            </button>
          </li>
        </ul>
      </nav>
    );
  }
}
