import React, { Component } from "react";
import { Container, Navbar, Form, Button } from "react-bootstrap";
import List from "./List";
import ListInfo from "./ListInfo";

let randomString = require("randomstring");
export default class PersonalInfo extends Component {
  constructor() {
    super();
    this.state = {
      id: "",
      name: "",
      age: "",
      gender:"Male",
      student: "",
      teacher: "",
      developer: "",
      createAt: "",
      updateAt: "",
      people: [],
    };
    // console.log(this.state.id);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleAgeChange = this.handleAgeChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOptionChange = this.handleOptionChange.bind(this);
    this.getInitialState = this.getInitialState.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleCheck1 = this.handleCheck1.bind(this);
    this.handleCheck2 = this.handleCheck2.bind(this);
  }
  //name
  handleNameChange = (event) => {
    this.setState({
      name: event.target.value,
    });
  };
  //age
  handleAgeChange = (event) => {
    this.setState({
      age: event.target.value,
    });
  };

  // start radio
  getInitialState = () => {
    return {
      gender: "",
    };
  };

  handleOptionChange = (changeEvent) => {
    this.setState({
      selectedOption: changeEvent.target.value,
    });
  };
  // end radio

  //start cheackbox
  handleCheck = (event) => {
    this.setState({
      student: event.target.checked ? "Student" : "",
    });
  };
  handleCheck1 = (event) => {
    this.setState({
      teacher: event.target.checked ? "Teacher" : "",
    });
  };
  handleCheck2 = (event) => {
    this.setState({
      developer: event.target.checked ? "Developer" : "",
    });
  };
  //end checkbox

  //sumit
  handleSubmit = (event) => {
    // alert(
    //   `${this.state.name} ${this.state.age}  ${this.state.selectedOption} ${this.state.student} ${this.state.teacher} ${this.state.developer}`
    // );
    // event.preventDefault();
    let object = {
      id: randomString.generate({
        length: 4,
        charset: "alphanumeric",
      }),
      name: this.state.name,
      age: this.state.age,
      student: this.state.student,
      teacher: this.state.teacher,
      developer: this.state.developer,
      createAt: this.state.createAt,
      updateAt: this.state.updateAt,
    };
    this.setState({
      people: [...this.state.people, object],
    });
  };
  render() {
    return (
      <div>
        <div style={{ width: "80%", margin: "auto" }}>
          <div style={{ backgroundColor: "black" }}>
            <h1
              style={{ textAlign: "center", padding: "12px", color: "white" }}
            >
              Personal Information
            </h1>
          </div>
        </div>

        <Form 
        // onSubmit={this.handleSubmit}
        >
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <Form.Group>
                  <br />
                  <Form.Label>Name:</Form.Label>
                  <Form.Control
                    size="lg"
                    type="text"
                    value={this.state.name}
                    onChange={this.handleNameChange}
                    placeholder="Input name"
                  />
                  <br />
                  <Form.Label>Age:</Form.Label>
                  <Form.Control
                    size="lg"
                    type="text"
                    value={this.state.age}
                    onChange={this.handleAgeChange}
                    placeholder="Input age"
                  />
                  <br />
                </Form.Group>
              </div>

              <div className="col-md-6">
                <Form.Group>
                  <br />
                  <Form.Label>Gender:</Form.Label>

                  <div className="radio">
                    <div className="container">
                      <div className="row">
                        <div className="col-ml-3">
                          <label>
                            <input
                              type="radio"
                              value="Male"
                              name="gender"
                              checked={this.state.selectedOption === "Male"}
                              onChange={this.handleOptionChange}
                            />
                            &nbsp;Male
                          </label>
                        </div>
                        &nbsp;&nbsp;
                        <div className="col-ml-3">
                          <label>
                            <input
                              type="radio"
                              value="Female"
                              name="gender"
                              checked={this.state.selectedOption === "Female"}
                              onChange={this.handleOptionChange}
                            />
                            &nbsp;Female
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <br />
                  <Form.Label>Job:</Form.Label>
                  <div className="checkbox">
                    <div className="container">
                      <div className="row">
                        <div className="col-ml3">
                          <label>
                            <input
                              type="checkbox"
                              name="job"
                              checked={this.state.student}
                              onChange={this.handleCheck}
                            />{" "}
                            Student
                          </label>
                        </div>
                        &nbsp;&nbsp;
                        <div className="col-ml3">
                          <label>
                            <input
                              type="checkbox"
                              name="job"
                              checked={this.state.teacher}
                              onChange={this.handleCheck1}
                            />{" "}
                            Teacher
                          </label>
                        </div>
                        &nbsp;&nbsp;
                        <div className="col-ml3">
                          <label>
                            <input
                              type="checkbox"
                              name="job"
                              checked={this.state.developer}
                              onChange={this.handleCheck2}
                            />{" "}
                            Developer
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </Form.Group>
              </div>
            </div>

            <Button
              variant="primary"
              onClick={(e) => this.handleSubmit()}
            >
              {" "}
              Submit{" "}
            </Button>

            <br />
            <Container>
              <div
                style={{
                  textAlignVertical: "center",
                  textAlign: "center",
                  justifyContent: "center",
                  alignItems: "center",
                }}
                className="row"
              >
                <br />
                <div className="col-ml-3">
                  <h5>Display data:</h5>
                </div>
                &nbsp;
                <div className="col-ml-3">
                  <Button variant="outline-secondary">List</Button>{" "}
                  <Button variant="outline-secondary">Card</Button>{" "}
                </div>
              </div>
            </Container>
          </div>
        </Form>
        <ListInfo people={this.state.people}/>
      </div>
    );
  }
}

const Textstyle = {
  textAlign: "center",
};
